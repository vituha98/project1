import 'package:clean_arch/src/core/utils/constants.dart';
import 'package:clean_arch/src/data/datasources/locale/app_database.dart';
import 'package:clean_arch/src/data/datasources/remote/news_api_service.dart';
import 'package:clean_arch/src/data/datasources/locale/app_database.dart';
import 'package:clean_arch/src/data/repositories/articles_repository_imp.dart';
import 'package:clean_arch/src/domain/repositories/articles_repository.dart';
import 'package:clean_arch/src/domain/usecases/get_articles_usecase.dart';
import 'package:clean_arch/src/domain/usecases/get_saved_articles_usecase.dart';
import 'package:clean_arch/src/domain/usecases/remove_article_usecase.dart';
import 'package:clean_arch/src/domain/usecases/save_article_usecase.dart';
import 'package:clean_arch/src/presentation/blocs/bloc/local_articles_bloc.dart';
import 'package:clean_arch/src/presentation/blocs/bloc/remote_articles_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:dio/dio.dart';

final injector = GetIt.instance;

Future<void> initializeDependencies() async {
  final database =
      await $FloorAppDatabase.databaseBuilder(kDatabaseName).build();
  injector.registerSingleton<AppDatabase>(database);

  // Dio.client
  injector.registerSingleton<Dio>(Dio());

  // Dependencies
  injector.registerSingleton<NewsApiService>(NewsApiService(injector()));

  injector.registerSingleton<ArticlesRepositoryImpl>(
      ArticlesRepositoryImpl(injector(), injector()));

  //useCases
  injector
      .registerSingleton<GetArticlesUseCase>(GetArticlesUseCase(injector()));

  injector.registerSingleton<GetSavedArticlesUseCase>(
      GetSavedArticlesUseCase(injector()));
  injector
      .registerSingleton<SaveArtilceUseCase>(SaveArtilceUseCase(injector()));
  injector.registerSingleton<RemoveArticleUseCase>(
      RemoveArticleUseCase(injector()));

  //blocs
  injector.registerFactory<RemoteArticlesBloc>(
      () => RemoteArticlesBloc(injector()));

  injector.registerFactory<LocalArticlesBloc>(
      () => LocalArticlesBloc(injector(), injector(), injector()));
}

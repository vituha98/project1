part of 'local_articles_bloc.dart';

abstract class LocalArticlesEvent extends Equatable {
  final Article? article;
  const LocalArticlesEvent({this.article});

  @override
  List<Object> get props => [];
}

class GetAllSavedArticlesEvent extends LocalArticlesEvent {
  const GetAllSavedArticlesEvent() : super(article: null);
}

class RemoveArticleEvent extends LocalArticlesEvent {
  const RemoveArticleEvent(Article article) : super(article: article);

  @override
  List<Object> get props => [article!];
}

class SaveArtilceEvent extends LocalArticlesEvent {
  const SaveArtilceEvent(Article article) : super(article: article);

  @override
  List<Object> get props => [article!];  
}

import 'package:bloc/bloc.dart';
import 'package:clean_arch/src/core/params/article_request.dart';
import 'package:clean_arch/src/core/resources/data_state.dart';
import 'package:clean_arch/src/domain/usecases/get_articles_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:dio/dio.dart';

import '../../../domain/entities/article.dart';

part 'remote_articles_event.dart';
part 'remote_articles_state.dart';

class RemoteArticlesBloc
    extends Bloc<RemoteArticlesEvent, RemoteArticlesState> {
  final GetArticlesUseCase _getArticlesUseCase;
  final List<Article> _articles = [];
  BlocProcessState blocProcessState = BlocProcessState.idle;
  int _page = 1;
  static const int _pageSize = 20;
  RemoteArticlesBloc(this._getArticlesUseCase)
      : super(const RemoteArticlesLoading()) {
    on<GetArticlesEvent>((event, emit) async {
      final dataState =
          await _getArticlesUseCase(params: ArticlesRequestParams(page: _page));
      if (dataState is DataSuccess && dataState.data!.isNotEmpty) {
        blocProcessState = BlocProcessState.busy;
        final articles = dataState.data;
        final noMoreData = articles!.length < _pageSize;
        _articles.addAll(articles);
        _page++;
        emit(RemoteArticlesDone(_articles, noMoreData: noMoreData));
        blocProcessState = BlocProcessState.idle;
      }
      if (dataState is DataFailed) {
        blocProcessState = BlocProcessState.idle;
        emit(RemoteArticlesError(dataState.error!));
        blocProcessState = BlocProcessState.busy;
      }
    });
  }
}

enum BlocProcessState { idle, busy }

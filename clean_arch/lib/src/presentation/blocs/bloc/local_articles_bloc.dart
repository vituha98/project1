import 'package:bloc/bloc.dart';
import 'package:clean_arch/src/domain/usecases/get_saved_articles_usecase.dart';
import 'package:clean_arch/src/domain/usecases/remove_article_usecase.dart';
import 'package:clean_arch/src/domain/usecases/save_article_usecase.dart';
import 'package:equatable/equatable.dart';

import '../../../domain/entities/article.dart';

part 'local_articles_event.dart';
part 'local_articles_state.dart';

class LocalArticlesBloc extends Bloc<LocalArticlesEvent, LocalArticlesState> {
  final GetSavedArticlesUseCase _getSavedArticlesUseCase;
  final SaveArtilceUseCase _saveArtilceUseCase;
  final RemoveArticleUseCase _removeArticleUseCase;
  LocalArticlesBloc(this._getSavedArticlesUseCase, this._saveArtilceUseCase,
      this._removeArticleUseCase)
      : super(const LocalArticlesLoading()) {
    on<GetAllSavedArticlesEvent>((event, emit) async {
      emit(await _getAllSavedArticles());
    });
    on<RemoveArticleEvent>((event, emit) async {
      await _removeArticleUseCase(params: event.article!);
      emit(await _getAllSavedArticles());
    });
    on<SaveArtilceEvent>(
      (event, emit) async {
        await _saveArtilceUseCase(params: event.article!);
        emit(await _getAllSavedArticles());
      },
    );
  }

  Future<LocalArticlesState> _getAllSavedArticles() async {
    final articles = await _getSavedArticlesUseCase(params: () {});
    return LocalArticlesDone(articles: articles);
  }
}

const String kMaterialAppTitle = 'Flutter Clean Architecture';

const String kBaseUrl = 'https://newsapi.org/v2';

const String kApiKey = '742847df3a3c419c8d637b78dbd93e4a';

const String kArticlesTableName = 'articles_table';

const String kDatabaseName = 'app_database.db';

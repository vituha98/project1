import 'package:floor/floor.dart';

import '../../../../domain/entities/source.dart';

class SourceTypeConverter extends TypeConverter<Source, String> {
  @override
  Source decode(String databaseValue) {
    final List<String> results = databaseValue.split(',');
    return Source(id: results.first, name: results.last.trim());
  }

  @override
  String encode(Source value) => '${value.id}, ${value.name}';
}

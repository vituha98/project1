import 'package:clean_arch/src/data/models/source_model.dart';
import 'package:clean_arch/src/domain/entities/article.dart';

class ArticleModel extends Article {
  const ArticleModel({
    int? id,
    required SourceModel soure,
     String? author,
    required String title,
     String? description,
    required String url,
     String? urlToImage,
    required String publishedAt,
     String? content,
  }) : super(
            id: id,
            author: author,
            title: title,
            source: soure,
            description: description,
            url: url,
            urlToImage: urlToImage,
            publishedAt: publishedAt,
            content: content);

  factory ArticleModel.fromJson(Map<String, dynamic> map) => ArticleModel(
      soure: SourceModel.fromJson(map['source'] as Map<String, dynamic>),
      author: map['author'] as String?,
      title: map['title'] as String,
      description: map['description'] as String?,
      url: map['url'] as String,
      urlToImage: map['urlToImage'] as String?,
      publishedAt: map['publishedAt'] as String,
      content: map['content'] as String?);
}

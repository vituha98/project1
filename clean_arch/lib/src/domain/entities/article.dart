import 'package:clean_arch/src/core/utils/constants.dart';
import 'package:equatable/equatable.dart';
import 'package:floor/floor.dart';

import 'package:clean_arch/src/domain/entities/source.dart';

@Entity(tableName: kArticlesTableName)
class Article extends Equatable {
  @PrimaryKey(autoGenerate: true)
  final int? id;
  final String? author;
  final Source source;
  final String title;
  final String? description;
  final String url;
  final String? urlToImage;
  final String publishedAt;
  final String? content;

  const Article({
     this.author,
    required this.id,
    required this.source,
    required this.title,
     this.description,
    required this.url,
     this.urlToImage,
    required this.publishedAt,
     this.content,
  });

  @override
  List<Object> get props {
    return [
      title,
      source,
      
      
      url,
      
      publishedAt,
      
    ];
  }

  @override
  bool? get stringify => true;
}

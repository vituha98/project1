import 'package:clean_arch/src/core/params/article_request.dart';
import 'package:clean_arch/src/core/resources/data_state.dart';
import 'package:clean_arch/src/core/usecases/usecase.dart';
import 'package:clean_arch/src/domain/entities/article.dart';

import '../../data/repositories/articles_repository_imp.dart';

class GetArticlesUseCase
    implements UseCase<DataState<List<Article>>, ArticlesRequestParams> {
  final ArticlesRepositoryImpl _articlesRepository;

  GetArticlesUseCase(this._articlesRepository);

  @override
  Future<DataState<List<Article>>> call({required ArticlesRequestParams params}) {
    return _articlesRepository.getBreakingNewsArticles(params);
  }

  
}

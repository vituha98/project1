import '../../core/usecases/usecase.dart';
import '../../data/repositories/articles_repository_imp.dart';
import '../entities/article.dart';

class RemoveArticleUseCase implements UseCase<void, Article> {
  final ArticlesRepositoryImpl _articlesRepository;

  RemoveArticleUseCase(this._articlesRepository);

  @override
  Future<void> call({required Article params}) {
    return _articlesRepository.removeArticle(params);
  }
}

import '../../core/usecases/usecase.dart';
import '../../data/repositories/articles_repository_imp.dart';
import '../entities/article.dart';

class GetSavedArticlesUseCase implements UseCase<List<Article>, void> {
  final ArticlesRepositoryImpl _articlesRepository;

  GetSavedArticlesUseCase(this._articlesRepository);

  @override
  Future<List<Article>> call({required void params}) {
    return _articlesRepository.getSavedArticles();
  }
}

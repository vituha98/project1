import '../../core/usecases/usecase.dart';
import '../../data/repositories/articles_repository_imp.dart';
import '../entities/article.dart';

class SaveArtilceUseCase implements UseCase<void, Article> {
  final ArticlesRepositoryImpl _articlesRepository;

  SaveArtilceUseCase(this._articlesRepository);

  @override
  Future<void> call({required Article params}) {
    return _articlesRepository.saveArticle(params);
  }
}
